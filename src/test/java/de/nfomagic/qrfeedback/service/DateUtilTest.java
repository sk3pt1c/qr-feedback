package de.nfomagic.qrfeedback.service;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.nfomagic.qrfeedback.business.services.DateUtil;

public class DateUtilTest {

	DateUtil dateUtil;

	@Before
	public void setUp() throws Exception {
		dateUtil = new DateUtil();
	}

	@After
	public void tearDown() throws Exception {
		dateUtil = null;
	}

	@Test
	public void testRetrieveActualDate() {

		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
		Calendar c = Calendar.getInstance();
		Date date = c.getTime();

		assertEquals(dateFormat.format(date), dateUtil.retrieveActualDate());
	}

	@Test
	public void testCreateCalendarList() {

		List<Date> dates = dateUtil.createCalendarDaysDescending(10);
		DateFormat formatter = new SimpleDateFormat("dd.MM.yy");

		Calendar date = Calendar.getInstance();
		date.roll(Calendar.DATE, false);

		assertEquals(formatter.format(date.getTime()), formatter.format(dates.get(1)));
	}

	@Test
	public void testGetThirtyDaysBefore() {
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
		Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		assertEquals("22.12.15", dateFormat.format(date));
	}

}
