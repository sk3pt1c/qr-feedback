package de.nfomagic.qrfeedback.presenation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.producer.SelectedQuestionBean;

@Named
public class MenuBean {

	@Inject
	private QuestionRepository questionRepository;

	@Inject
	private SelectedQuestionBean selectedQuestionBean;

	@Inject
	FacesContext facesContext;

	private Question selectedQuestion;

	private MenuModel model;

	private List<Question> questions = new ArrayList<Question>();

	@PostConstruct
	public void init() {

		questions = questionRepository.findAllActiveByMember();

		model = new DefaultMenuModel();
		model.addElement(new DefaultMenuItem("Dashboard", "ui-icon-dashboard", "dashboard.jsf"));

		for (Question q : questions) {
			MenuItem questionMenuItem = new DefaultMenuItem(q.getQuestionText(), "ui-icon-comment");
			model.addElement(questionMenuItem);
		}

	}

	public void redirectToQuestionDetail(Question q) {
		selectedQuestionBean.setSelectedQuestion(q);
		try {
			facesContext.getExternalContext().redirect("createFeedbackCards.jsf");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onSelect(SelectEvent event) {

	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}

}
