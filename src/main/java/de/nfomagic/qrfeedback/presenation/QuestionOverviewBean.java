package de.nfomagic.qrfeedback.presenation;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.presenation.charts.BarModelWrapper;

@Named
@ViewScoped
public class QuestionOverviewBean {

	@Inject
	QuestionRepository questionRepository;

	private List<Question> questions;

	private List<BarModelWrapper> barModels;

	BarModelWrapper barModelWrapper;

	private BarChartModel barModel;

	@PostConstruct
	public void init() {
		questions = questionRepository.findAllActiveByMember();
		barModels = new ArrayList<BarModelWrapper>();
		for (Question q : questions) {
			barModels.add(new BarModelWrapper(q, createBarModel(q)));
		}
	}
	
	public void deleteQuestion(){
		
	}

	// REFACTORING: Klasse BarModelHandler erstellen --> createBarModel und
	// getMaxAnswerCount kapseln!
	private BarChartModel createBarModel(Question question) {

		List<Answer> answers = question.getAnswers();

		barModel = new BarChartModel();

		Axis xAxis = barModel.getAxis(AxisType.X);
		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setMin(0);

		// important for right scaling
		yAxis.setMax(getMaxAnswerCount(question.getAnswers()));

		ChartSeries chartSeries = new ChartSeries();
		for (Answer a : answers) {
			;
			chartSeries.set(a.getAnswerText(), a.getCounter());

			// Alternativ
			// chartSeries.setLabel(a.getAnswerText());
			// chartSeries.set(a.getCounter());
		}

		barModel.addSeries(chartSeries);

		return barModel;
	}

	private int getMaxAnswerCount(List<Answer> answers) {

		int maxAnswerCount = 0;

		for (Answer a : answers) {
			if (a.getCounter() > maxAnswerCount) {
				maxAnswerCount = a.getCounter();
			}
		}

		if (maxAnswerCount > 10) {
			maxAnswerCount = (int) (maxAnswerCount * 1.1);
		} else if (maxAnswerCount <= 10) {
			maxAnswerCount = maxAnswerCount + 1;
		}

		return maxAnswerCount;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public List<BarModelWrapper> getBarModels() {
		return barModels;
	}

	public void setBarModels(List<BarModelWrapper> barModels) {
		this.barModels = barModels;
	}

}
