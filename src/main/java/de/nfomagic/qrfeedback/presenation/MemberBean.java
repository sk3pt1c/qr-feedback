package de.nfomagic.qrfeedback.presenation;

import java.io.Serializable;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;
import de.nfomagic.qrfeedback.business.services.UserIdentityService;

/**
 * Backing bean for Member entities.
 * <p/>
 * This class provides CRUD functionality for all Member entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class MemberBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	UserIdentityService memberSerivce;

	@Inject
	FacesContext facesContext;

	private UserIdentity member;

	/**
	 * @return the member
	 */
	public UserIdentity getMember() {
		return member;
	}

	/**
	 * @param member
	 *            the member to set
	 */
	public void setMember(UserIdentity member) {
		this.member = member;
	}

}
