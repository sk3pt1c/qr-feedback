package de.nfomagic.qrfeedback.presenation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.UserIdentityRepository;
import de.nfomagic.qrfeedback.business.services.QuestionService;
import de.nfomagic.qrfeedback.qualifiers.ActualUser;

/**
 * Backing bean for Question entities.
 * <p/>
 * This class provides CRUD functionality for all Question entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@ViewScoped
public class CreateQuestionBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Question entities
	 */

	@Inject
	QuestionService questionService;

	@Inject
	Logger log;

	@Inject
	FacesContext facesContext;

	private String textAreaContent;

	private Question newQuestion;

	@Inject
	@ActualUser
	private UserIdentity actualUser;

	@PostConstruct
	private void initQuestion() {
		newQuestion = new Question(actualUser);
		newQuestion.setActive(true);
	}

	public String createQuestion() {
		try {
			newQuestion.setAnswers(mapAnswersFromTextArea());
			questionService.createQuestion(newQuestion);
			return "dashboard";

		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage,
					"Fehler! Der Frage müssen mindestens 2 und höchstens 4 Antworten zugeordnet werden."));

		}
		return "";
	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Creation of question failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

	/**
	 * @return the newQuestion
	 */
	public Question getNewQuestion() {
		return newQuestion;
	}

	/**
	 * @param newQuestion
	 *            the newQuestion to set
	 */
	public void setNewQuestion(Question newQuestion) {
		this.newQuestion = newQuestion;
	}

	public String getTextAreaContent() {
		return textAreaContent;
	}

	public void setTextAreaContent(String textAreaContent) {
		this.textAreaContent = textAreaContent;
	}

	public List<Answer> mapAnswersFromTextArea() {
		String[] answerStrings;
		answerStrings = textAreaContent.split("\n");
		List<Answer> answers = new ArrayList<Answer>();
		for (String s : answerStrings) {
			Answer a = new Answer(s);
			answers.add(a);
			log.info("Added answer " + a.getId() + " with text " + a.getAnswerText());
		}
		return answers;
	}
	
}
