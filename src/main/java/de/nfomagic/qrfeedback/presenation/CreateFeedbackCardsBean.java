package de.nfomagic.qrfeedback.presenation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.business.services.FeedbackCardService;
import de.nfomagic.qrfeedback.qualifiers.SelectedQuestion;

/**
 * Backing bean for Question entities.
 * <p/>
 * This class provides CRUD functionality for all Question entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@ViewScoped
public class CreateFeedbackCardsBean implements Serializable {

	private static final String DESTINATION_PATH = System.getProperty("jboss.server.data.dir", "uploads") + "\\";

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving FeedbackCard entities
	 */

	@Inject
	Logger log;

	@Inject
	FacesContext facesContext;

	@Inject
	FeedbackCardService feedbackCardService;

	@Inject
	QuestionRepository questionRepository;

	private StreamedContent file;

	@Inject
	@SelectedQuestion
	private Question question;

	private int feedbackCardAmount;

	private boolean singleUsage;

	public void generateFeedbackCards() {

		// InputStream stream = ((ServleWtContext) FacesContext
		// .getCurrentInstance().getExternalContext().getContext())
		// .getResourceAsStream("/resources/demo/images/optimus.jpg");
		// file = new DefaultStreamedContent(stream, "image/jpg",
		// "downloaded_optimus.jpg");

		log.info("Question is " + question);

		try {
			feedbackCardService.createFeedbackCards(question, feedbackCardAmount, singleUsage);
			initFile();
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage,
					"Fehler! FeedbackCards können nicht erstellt werden."));
			e.printStackTrace();
		}

	}

	public void initFile() {
		log.info("Path to Exile: " + DESTINATION_PATH + "FeedbackCards.pptx");
		// InputStream stream = ((ServletContext)
		// FacesContext.getCurrentInstance().getExternalContext().getContext())
		// .getResourceAsStream(DESTINATION_PATH + "FeedbackCards.pptx");

		InputStream stream = null;
		try {
			stream = new FileInputStream(DESTINATION_PATH + "FeedbackCards.pptx");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Stream is " + stream);
		file = new DefaultStreamedContent(stream,
				"application/vnd.openxmlformats-officedocument.presentationml.presentation", "FeedbackCards.pptx");
		log.info("File is " + file);
	}

	public StreamedContent getFile() {
		return file;
	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Creation of FeedbackCard failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public int getFeedbackCardAmount() {
		return feedbackCardAmount;
	}

	public void setFeedbackCardAmount(int feedbackCardAmount) {
		this.feedbackCardAmount = feedbackCardAmount;
	}

	public boolean isSingleUsage() {
		return singleUsage;
	}

	public void setSingleUsage(boolean singleUsage) {
		this.singleUsage = singleUsage;
	}

}
