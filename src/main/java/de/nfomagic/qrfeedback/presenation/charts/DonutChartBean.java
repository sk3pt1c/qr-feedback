package de.nfomagic.qrfeedback.presenation.charts;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.primefaces.model.chart.DonutChartModel;

@Named
public class DonutChartBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DonutChartModel donutModel1;

	@PostConstruct
	public void init() {
		createDonutModels();
	}

	public DonutChartModel getDonutModel1() {
		return donutModel1;
	}

	private void createDonutModels() {
		donutModel1 = initDonutModel();
	}

	private DonutChartModel initDonutModel() {
		DonutChartModel model = new DonutChartModel();

		Map<String, Number> circle1 = new LinkedHashMap<String, Number>();
		circle1.put("Brand 1", 34);
		circle1.put("Brand 2", 66);

		model.addCircle(circle1);

		return model;
	}
}
