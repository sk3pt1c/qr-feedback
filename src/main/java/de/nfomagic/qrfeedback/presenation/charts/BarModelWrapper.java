package de.nfomagic.qrfeedback.presenation.charts;

import org.primefaces.model.chart.BarChartModel;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;

public class BarModelWrapper {

	private Question question;
	private BarChartModel barChartModel;

	public BarModelWrapper(Question question, BarChartModel barChartModel) {
		super();
		this.question = question;
		this.barChartModel = barChartModel;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public BarChartModel getBarChartModel() {
		return barChartModel;
	}

	public void setBarChartModel(BarChartModel barChartModel) {
		this.barChartModel = barChartModel;
	}

}
