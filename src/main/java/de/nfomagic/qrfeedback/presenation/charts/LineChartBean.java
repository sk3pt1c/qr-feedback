package de.nfomagic.qrfeedback.presenation.charts;

import java.io.Serializable;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.services.DateUtil;
import de.nfomagic.qrfeedback.business.services.ScanService;

@Named
@ViewScoped
public class LineChartBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LineChartModel lineModel;

	@Inject
	private ScanService scanService;

	@Inject
	Logger log;

	DateUtil dateUtil;

	@PostConstruct
	public void init() {
		lineModel = createLineModel();
	}

	public LineChartModel getLineModel() {
		return lineModel;
	}

	public void setLineModel(LineChartModel lineModel) {
		this.lineModel = lineModel;
	}

	private LineChartModel createLineModel() {

		lineModel = new LineChartModel();

		dateUtil = new DateUtil();
		Map<Question, Map<String, Integer>> questionMap = scanService.findScansForQuestionByDate();

		for (Map.Entry<Question, Map<String, Integer>> qMap : questionMap.entrySet()) {
			LineChartSeries series = new LineChartSeries();
			series.setLabel(qMap.getKey().getQuestionText());
			log.info("Added questionText \"" + qMap.getKey().getQuestionText() + "\" to series!");
			for (Map.Entry<String, Integer> m : qMap.getValue().entrySet()) {
				series.set(m.getKey(), m.getValue());
				log.info("Added key \"" + m.getKey() + "\" to series!");
			}
			lineModel.addSeries(series);
			log.info("Added series!");
		}

		lineModel.setZoom(true);
		lineModel.getAxis(AxisType.Y).setLabel("Anzahl an Scans");
		DateAxis axis = new DateAxis("Dates");
		axis.setTickAngle(-50);
		axis.setMax("2015-28-12");
		axis.setTickFormat("%b %#d, %y");

		lineModel.getAxes().put(AxisType.X, axis);

		return lineModel;
	}

}
