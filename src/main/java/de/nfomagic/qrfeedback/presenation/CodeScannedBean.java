package de.nfomagic.qrfeedback.presenation;

import java.io.Serializable;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.nfomagic.qrfeedback.business.objects.Scan;
import de.nfomagic.qrfeedback.business.objects.surveyModule.AnswerRating;
import de.nfomagic.qrfeedback.business.objects.surveyModule.AnswerReason;
import de.nfomagic.qrfeedback.business.repositories.ScanRepository;
import de.nfomagic.qrfeedback.business.services.AnswerRatingService;
import de.nfomagic.qrfeedback.business.services.AnswerReasonService;

/**
 * Backing bean for Question entities.
 * <p/>
 * This class provides CRUD functionality for all Question entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@SessionScoped
public class CodeScannedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	FacesContext facesContext;

	@Inject
	Logger log;

	@Inject
	private AnswerReasonService answerReasonService;

	@Inject
	private AnswerRatingService answerRatingService;

	@Inject
	private ScanRepository scanRepository;

	private Scan scan;

	private Integer rating;
	private String reason;

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@PostConstruct
	private void init() {

		Map<String, String> params = facesContext.getExternalContext().getRequestParameterMap();
		id = params.get("id");
		log.info("ID ist " + id);
	}

	// WAS WENN ERST DAS EINE UND DANN DAS ANDERE??? RATING -> SAVE -> REASON ->	// SAVE
	public void save() {
		scan = scanRepository.find(Long.parseLong(id));
		log.info("Found Scan with ID " + scan.getId());
		log.info("Is answerReason empty? " + reason.isEmpty());
		if (!reason.isEmpty()) {
			log.info("About to persist reason");
			answerReasonService.createAnswerReason(new AnswerReason(reason, scan));
		}
		log.info("Is answerRating null? " + rating.equals(null));
		if (!rating.equals(null)) {
			log.info("About to persist rating");
			answerRatingService.createAnswerRating(new AnswerRating(rating, scan));
		}
	}

}
