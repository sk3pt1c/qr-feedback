package de.nfomagic.qrfeedback.producer;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.qualifiers.SelectedQuestion;

@SessionScoped
@Named
public class SelectedQuestionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Question selectedQuestion;

	@Inject
	private Logger log;

	@PostConstruct
	private void initEmptySelection() {
		selectedQuestion = new Question();
	}

	@Produces
	@SelectedQuestion
	@Named("selectedQuestion")
	@RequestScoped
	public Question getSelectedQuestion() {
		return selectedQuestion;
	}

	public void setSelectedQuestion(Question selectedQuestion) {
		if (selectedQuestion != null) {
			log.info("SelectedQuestion has ID: " + selectedQuestion.getId());
			this.selectedQuestion = selectedQuestion;
		}
	}

	public void onListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Question answer) {

	}

}
