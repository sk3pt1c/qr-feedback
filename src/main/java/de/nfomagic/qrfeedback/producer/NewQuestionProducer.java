package de.nfomagic.qrfeedback.producer;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.qualifiers.ActualUser;
import de.nfomagic.qrfeedback.qualifiers.NewQuestion;

/**
 * Backing bean for Survey entities.
 * <p/>
 * This class provides CRUD functionality for all Survey entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */
@RequestScoped
public class NewQuestionProducer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Produces
	@Named
	@NewQuestion
	private Question newQuestion;

	@Inject
	@ActualUser
	private UserIdentity actualUser;

	@PostConstruct
	private void initNewQuestion() {
		newQuestion = new Question();
		newQuestion.setUser(actualUser);
	}

	/**
	 * @return the newQuestion
	 */
	public Question getNewQuestion() {
		return newQuestion;
	}

}