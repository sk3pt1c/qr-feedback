/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.nfomagic.qrfeedback.producer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.presenation.charts.BarModelWrapper;

@RequestScoped
public class ChartDataProducer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	Logger log;

	private List<BarModelWrapper> barModels;

	private List<Question> questions;

	BarModelWrapper barModelWrapper;

	@Inject
	private QuestionRepository questionRepository;

	private BarChartModel barModel;

	@PostConstruct
	public void retrieveAnswerByQuestion() {
		questions = questionRepository.findAllActiveByMember();
		log.info("Anzahl Fragen: " + questions.size());
		barModels = new ArrayList<BarModelWrapper>();
		for (Question q : questions) {
			barModels.add(new BarModelWrapper(q, createBarModel(q)));
		}
	}

	// @Named provides access the return value via the EL variable name
	// "questions" in the UI (e.g.
	// Facelets or JSP view)
	@Produces
	@Named
	public List<Question> getAnswers() {
		return questions;
	}

	private BarChartModel createBarModel(Question question) {

		List<Answer> answers = question.getAnswers();

		barModel = new BarChartModel();

		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setMin(0);

		// important for right scaling
		yAxis.setMax(getMaxAnswerCount(question.getAnswers()));

		ChartSeries chartSeries = new ChartSeries();
		for (Answer a : answers) {
			;
			chartSeries.set(a.getAnswerText(), a.getCounter());

			// Alternativ
			// chartSeries.setLabel(a.getAnswerText());
			// chartSeries.set(a.getCounter());
		}

		barModel.addSeries(chartSeries);

		return barModel;
	}

	private int getMaxAnswerCount(List<Answer> answers) {

		int maxAnswerCount = 0;

		for (Answer a : answers) {
			if (a.getCounter() > maxAnswerCount) {
				maxAnswerCount = a.getCounter();
			}
		}

		if (maxAnswerCount > 10) {
			maxAnswerCount = (int) (maxAnswerCount * 1.1);
		} else if (maxAnswerCount <= 10) {
			maxAnswerCount = maxAnswerCount + 1;
		}

		return maxAnswerCount;
	}

	public void onAnswerListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Question answer) {
		retrieveAnswerByQuestion();
	}

}
