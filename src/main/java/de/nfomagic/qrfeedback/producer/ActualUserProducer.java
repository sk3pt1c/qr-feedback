package de.nfomagic.qrfeedback.producer;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;
import de.nfomagic.qrfeedback.qualifiers.ActualUser;

@SessionScoped
public class ActualUserProducer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserIdentity actualUser;

	@Produces
	@ActualUser
	@Named("actualUser")
	@RequestScoped
	public UserIdentity getActualUser() {
		return actualUser;
	}

	public void setActualUser(UserIdentity actualUser) {
		this.actualUser = actualUser;
	}

}
