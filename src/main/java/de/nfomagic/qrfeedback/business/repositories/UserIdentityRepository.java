package de.nfomagic.qrfeedback.business.repositories;

/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;

@ApplicationScoped
public class UserIdentityRepository {

	@Inject
	private EntityManager em;

	public UserIdentity findById(int id) {
		return em.find(UserIdentity.class, id);
	}

	public UserIdentity findByUsername(String loginName) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UserIdentity> criteria = cb.createQuery(UserIdentity.class);
		Root<UserIdentity> Member = criteria.from(UserIdentity.class);
		criteria.select(Member).where(
				cb.equal(Member.get("loginName"), loginName));
		return em.createQuery(criteria).getSingleResult();
	}

	// TODO: Implementieren
//	public Member findByEmail(String email) {
//		CriteriaBuilder cb = em.getCriteriaBuilder();
//		CriteriaQuery<Member> criteria = cb.createQuery(Member.class);
//		Root<Member> member = criteria.from(Member.class);
//		// Swap criteria statements if you would like to try out type-safe
//		// criteria queries, a new
//		// feature in JPA 2.0
//		// criteria.select(member).where(cb.equal(member.get(Member_.name),
//		// email));
//		criteria.select(member).where(cb.equal(member.g.get("email"), email));
//		return em.createQuery(criteria).getSingleResult();
//	}

}
