package de.nfomagic.qrfeedback.business.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;

@ApplicationScoped
public class FeedbackCardRepository {

	@Inject
	private EntityManager em;

	@Inject
	QuestionRepository questionRepository;

	public FeedbackCard findById(long id) {
		return em.find(FeedbackCard.class, id);
	}

	public List<FeedbackCard> findByQuestion(long questionId) {
		TypedQuery<FeedbackCard> query = em
				.createQuery("SELECT f FROM FeedbackCard f WHERE f.question.id = :questionId", FeedbackCard.class)
				.setParameter("questionId", questionId);
		return query.getResultList();
	}

	// Refactoring überlegen --> Auslagern in Service- bzw. Viewlayer
	@Named(value = "findFeedbackCardsByUser")
	@Produces
	@RequestScoped
	public List<FeedbackCard> findAll() {

		List<Question> allQuestionsByMember = questionRepository.findAllActiveByMember();
		List<FeedbackCard> allFeedbackCardsByMember = new ArrayList<FeedbackCard>();

		for (Question q : allQuestionsByMember) {
			allFeedbackCardsByMember.addAll(findByQuestion(q.getId()));
		}

		return allFeedbackCardsByMember;
	}

}
