package de.nfomagic.qrfeedback.business.repositories;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import de.nfomagic.qrfeedback.business.objects.Scan;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;

@ApplicationScoped
public class ScanRepository {

	@Inject
	private EntityManager em;

	@Inject 
	private Logger log;
	
	public Scan find(long id) {
		return em.find(Scan.class, id);
	}

	@Produces
	@Named("allScans")
	public List<Scan> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Scan> criteria = cb.createQuery(Scan.class);
		criteria.from(Scan.class);
		return em.createQuery(criteria).getResultList();
	}

	public String findFirstScanForQuestion(Question question) {
		TypedQuery<Scan> query = em
				.createQuery("SELECT MIN(s.timestamp) FROM Scan s WHERE s.answer.question.id = :questionId", Scan.class)
				.setParameter("questionId", question.getId());
		Scan firstScanForQuestion = query.getSingleResult();
		DateFormat formatter = new SimpleDateFormat("dd.MM.yy");
		return formatter.format(firstScanForQuestion.getTimestamp());
	}

	public String findLastScanForQuestion(Question question) {
		TypedQuery<Scan> query = em
				.createQuery("SELECT MAX(s.timestamp) FROM Scan s WHERE s.answer.question.id = :questionId", Scan.class)
				.setParameter("questionId", question.getId());
		Scan lastScanForQuestion = query.getSingleResult();
		DateFormat formatter = new SimpleDateFormat("dd.MM.yy");
		return formatter.format(lastScanForQuestion.getTimestamp());
	}

	public List<Scan> findAllScansByQuestion(Question q) {
		TypedQuery<Scan> query = em
				.createQuery("SELECT s FROM Scan s WHERE s.answer.question.id = :questionId", Scan.class)
				.setParameter("questionId", q.getId());
		List<Scan> scansByQuestion = query.getResultList();
		log.info("Found " + scansByQuestion.size() + " scans for question " + q.getQuestionText());
		return scansByQuestion;
	}

}
