package de.nfomagic.qrfeedback.business.repositories;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.qualifiers.ActualUser;
import de.nfomagic.qrfeedback.qualifiers.AllSurveysFromUser;

@ApplicationScoped
@Named
public class QuestionRepository {

	@Inject
	EntityManager em;

	@Inject
	@ActualUser
	private UserIdentity actualUser;

	public Question find(long id) {
		return em.find(Question.class, id);
	}

	@Produces
	@RequestScoped
	@AllSurveysFromUser
	@Named("allActiveSurveysByMember")
	public List<Question> findAllActiveByMember() {
		TypedQuery<Question> query = em.createQuery(
				"SELECT q FROM Question q WHERE q.user.id = :userId AND q.active = true ORDER BY q.creationDate",
				Question.class).setParameter("userId", actualUser.getId());
		List<Question> questionsFromUser = query.getResultList();

		return questionsFromUser;

	}

	@Produces
	@Named(value = "mostPopularQuestions")
	@RequestScoped
	public List<Question> findMostScannedQuestions() {
		List<Question> questions = findAllActiveByMember();

		Comparator<Question> byTotalAnswerCount = new Comparator<Question>() {
			@Override
			public int compare(Question q1, Question q2) {
				return q1.getTotalAnswerCount().compareTo(q2.getTotalAnswerCount());
			}
		};

		// Sort ascending
		Collections.sort(questions, byTotalAnswerCount);

		// Reverse for descending order
		Collections.reverse(questions);

		return questions;
	}

}
