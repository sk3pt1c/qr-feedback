package de.nfomagic.qrfeedback.business.repositories;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;
import de.nfomagic.qrfeedback.business.objects.QrCode;

@ApplicationScoped
public class QrCodeRepository {

	@Inject
	private EntityManager em;

	public QrCode findByHash(String hash) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<QrCode> criteria = cb.createQuery(QrCode.class);
		Root<QrCode> qrCode = criteria.from(QrCode.class);
		criteria.select(qrCode).where(cb.equal(qrCode.get("hash"), hash));
		return em.createQuery(criteria).getSingleResult();
	}

	public List<QrCode> findByFeedbackCard(FeedbackCard feedbackCard) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<QrCode> criteria = cb.createQuery(QrCode.class);
		Root<QrCode> qrCode = criteria.from(QrCode.class);
		criteria.select(qrCode).where(
				cb.equal(qrCode.get("feedbackCard"), feedbackCard));
		return em.createQuery(criteria).getResultList();
	}
	
    public List<QrCode> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<QrCode> criteria = cb.createQuery(QrCode.class);
        Root<QrCode> qrCode = criteria.from(QrCode.class);
        return em.createQuery(criteria).getResultList();
    }
	

	public QrCode findById(long id) {
		return em.find(QrCode.class, id);
	}

}
