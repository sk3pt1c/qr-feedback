package de.nfomagic.qrfeedback.business.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;

@ApplicationScoped
public class AnswerRepository {

	@Inject
	EntityManager em;

	public Answer find(long id) {
		return em.find(Answer.class, id);
	}

	public List<Answer> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Answer> criteria = cb.createQuery(Answer.class);
		Root<Answer> answer = criteria.from(Answer.class);
		return em.createQuery(criteria).getResultList();
	}

	public List<Answer> findAllByQuestion(Question question) {
		List<Answer> answers = findAll();
		List<Answer> answersByQuestion = new ArrayList<Answer>();
		for (Answer a : answers) {
			if (a.getQuestion().getId() == question.getId()) {
				answersByQuestion.add(a);
			}
		}
		return answersByQuestion;
	}

}
