package de.nfomagic.qrfeedback.business.services;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@Named
public class PowerpointConverterService {

	private Client client;
	private WebTarget target;

	@PostConstruct
	protected void init() {
		client = ClientBuilder.newClient();
		// example query params: ?q=Turku&cnt=10&mode=json&units=metric
		target = client.target(" https://do.convertapi.com/Pdf2PowerPoint").queryParam("ApiKey", "603104681");
	}	
}
