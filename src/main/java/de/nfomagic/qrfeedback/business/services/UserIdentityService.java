/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.nfomagic.qrfeedback.business.services;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.User;

import de.nfomagic.qrfeedback.business.repositories.UserIdentityRepository;

@Stateless
@Named
public class UserIdentityService {

	@Inject
	private Logger log;

	@Inject
	private UserIdentityRepository memberRepository;

	@Inject
	PartitionManager partitionManager;

	public void register(User newUser, String password) throws Exception {

		IdentityManager identityManager = this.partitionManager
				.createIdentityManager();

		// Ohne explizites Mapping --> NoEntityMapperException ... Bug von
		// Picketlink???
		User user = new User(newUser.getLoginName());
		user.setEmail(newUser.getEmail());
		user.setFirstName(newUser.getFirstName());
		user.setLastName(newUser.getLastName());

		identityManager.add(user);
		identityManager.updateCredential(user, new Password(password));

		log.info(user.getLoginName() + " registred!");
	}

	public boolean isRegistred(String username) {
		try {
			memberRepository.findByUsername(username);
		} catch (NoResultException e) {
			return false;
		}
		return true;
	}

}
