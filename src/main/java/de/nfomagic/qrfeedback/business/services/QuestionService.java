/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.nfomagic.qrfeedback.business.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.business.repositories.UserIdentityRepository;
import de.nfomagic.qrfeedback.exceptions.WrongAnswerCountException;

@Stateless
@Named
public class QuestionService {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private QuestionRepository questionRepository;

	@Inject
	private UserIdentityRepository userIdentityRepository;

	@Inject
	private Event<Question> questionEventSource;

	public void createQuestion(Question question) throws WrongAnswerCountException {
		List<Answer> answers = question.getAnswers();
		if (answers == null || answers.isEmpty() || answers.size() < 2 || answers.size() > 4) {
			throw new WrongAnswerCountException();
		} else {

			// verhindert unsaved entity exception
			question.setUser(userIdentityRepository.findById(question.getUser().getId()));

			log.info("Has Question questionMark? " + question.getQuestionText().endsWith("?"));

			// Fragezeichen hinzufügen, falls keins eingetragen wurde
			if (question.getQuestionText().endsWith("?") == false) {
				log.info("No Questionmark! Adding one...");
				String questionText = question.getQuestionText();
				questionText += "?";
				question.setQuestionText(questionText);
			}

			log.info("Has Question questionMark? " + question.getQuestionText().endsWith("?"));

			em.persist(question);
			log.info(question.getQuestionText() + " created by "
					+ userIdentityRepository.findById(question.getUser().getId()).getLoginName());
			// questionEventSource.fire(question);
		}
	}

	public void deactiveQuestion(Long questionId) {
		Question questionToDeactivate = questionRepository.find(questionId);
		questionToDeactivate.setActive(false);
		em.merge(questionToDeactivate);
	}

	public void deleteQuestion(Question question) {
		question = questionRepository.find(question.getId());
		em.remove(question);
		// log.info(question.getTitle() + " removed by "
		// + question.getUser().getName());
	}

	public void updateQuestion(Question question) throws WrongAnswerCountException {
		question = questionRepository.find(question.getId());
		List<Answer> answers = question.getAnswers();
		if (answers == null || answers.isEmpty() || answers.size() < 2 || answers.size() > 4) {
			throw new WrongAnswerCountException();
		} else {
			em.merge(question);
			log.info(question.getQuestionText() + " updated by "
					+ userIdentityRepository.findById(question.getUser().getId()).getLoginName());
		}
	}

}
