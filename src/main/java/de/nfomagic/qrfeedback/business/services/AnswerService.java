/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.nfomagic.qrfeedback.business.services;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.exceptions.WrongAnswerCountException;
import de.nfomagic.qrfeedback.qualifiers.ActualUser;

@Stateless
@Named
public class AnswerService {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	@ActualUser
	private UserIdentity userIdentity;

	@Inject
	private Event<Answer> answerEventSource;

	public void createAnswer(Answer answer){
		em.persist(answer);
	}

	public void deleteAnswer(Answer answer) {
		em.remove(answer);
		// log.info(answer.getTitle() + " removed by "
		// + answer.getUser().getName());
	}

	public void updateAnswer(Answer answer){
		em.merge(answer);
	}

}