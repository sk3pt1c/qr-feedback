package de.nfomagic.qrfeedback.business.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;
import de.nfomagic.qrfeedback.business.objects.QrCode;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.AnswerRepository;
import de.nfomagic.qrfeedback.business.repositories.FeedbackCardRepository;
import de.nfomagic.qrfeedback.business.repositories.QrCodeRepository;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.exceptions.CardAlreadyScannedException;
import de.nfomagic.qrfeedback.exceptions.CodeAlreadyScannedException;

@Stateless
@Named
public class QrScanService {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private QuestionRepository questionRepository;

	@Inject
	FeedbackCardService feedbackCardService;

	@Inject
	FeedbackCardRepository feedbackCardRepository;

	@Inject
	private AnswerRepository answerRepositroy;

	@Inject
	QrCodeRepository qrCodeRepository;

	@Inject
	AnswerService answerService;

	public void processScan(QrCode qrCode) throws CodeAlreadyScannedException, CardAlreadyScannedException {

		FeedbackCard feedbackCard = feedbackCardRepository.findById(qrCode.getFeedbackCard().getId());

		Question question = questionRepository.find(feedbackCard.getQuestion().getId());
		Answer answer = answerRepositroy.find(qrCode.getAnswer().getId());

		if (question.isMultipleChoice()) {
			if (qrCode.isActive()) {
				if (feedbackCard.isSingleUsage()) {
					qrCode.setActive(false);
				}
			} else {
				throw new CodeAlreadyScannedException();
			}
		} else {
			if (qrCode.isActive()) {

				// increment counter
				int counter = answer.getCounter();
				log.info("COUNTER BEFORE UPDATE: " + answer.getCounter());
				answer.setCounter(counter + 1);
				log.info("COUNTER AFTER UPDATE: " + answer.getCounter());

				// Persist incremented counter
				em.merge(answer);

				if (!feedbackCard.isSingleUsage()) {
					// Deactive QR-Codes from FeedbackCard
					List<QrCode> qrCodesFromCard = qrCodeRepository.findByFeedbackCard(feedbackCard);
					for (QrCode q : qrCodesFromCard) {
						q.setActive(false);
						em.merge(q);
					}
				}

				log.info("----QR-CODE WITH HASH " + qrCode.getHash() + " SCANNED (" + qrCode.getAnswer().getCounter()
						+ ")!----");

			} else {
				throw new CardAlreadyScannedException();
			}
		}

	}
}