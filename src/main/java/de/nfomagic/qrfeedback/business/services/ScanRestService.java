package de.nfomagic.qrfeedback.business.services;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.picketlink.authorization.annotations.RolesAllowed;

import de.nfomagic.qrfeedback.business.objects.QrCode;
import de.nfomagic.qrfeedback.business.objects.Scan;
import de.nfomagic.qrfeedback.business.repositories.QrCodeRepository;
import de.nfomagic.qrfeedback.exceptions.CardAlreadyScannedException;
import de.nfomagic.qrfeedback.exceptions.CodeAlreadyScannedException;

@Path("/codes")
public class ScanRestService {

	@Inject
	private Logger log;

	@Inject
	QrCodeRepository qrCodeRepository;

	@Inject
	private ScanService scanService;

	private int id;

	@GET
	@Path("/{hash:[a-zA-z0-9]*}")
	@Produces("text/html")
	public Response lookupQrCodeByHash(@PathParam("hash") String hash, @Context HttpServletRequest request,
			@Context HttpServletResponse response, @HeaderParam("Content-Length") int length,
			@HeaderParam("user-agent") String userAgent, @HeaderParam("Cf-Connecting-Ip") String cfConnectionIp) {

		log.info("Lookup QrCode with hash: " + hash);
		log.info("Content-Length: " + length);
		log.info("User-Agent: " + userAgent);
		log.info("IP: " + cfConnectionIp);

		QrCode qrCode = qrCodeRepository.findByHash(hash);

		try {
			Scan scan = scanService.processScan(qrCode);

			try {
				id = scan.getId().intValue();
				// request.getRequestDispatcher(
				// "/codeScanned/codeScanned.xhtml?faces-redirect=true&includeViewParams=true")
				// .forward(request, response);
				String contextPath = request.getContextPath();
				response.sendRedirect(
						contextPath + "/codeScanned.jsf?id=" + scan.getId());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (CodeAlreadyScannedException e) {
			// TODO: Redirect to CodeAlreadyScanned page
		} catch (CardAlreadyScannedException e) {
			// TODO: Redirect to CardAlreadyScanned page
		}

		return Response.status(Status.ACCEPTED).build();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
