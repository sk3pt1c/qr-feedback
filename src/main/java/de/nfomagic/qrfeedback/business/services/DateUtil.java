package de.nfomagic.qrfeedback.business.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class DateUtil {

	public Calendar retrieveActualDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
		Calendar c = Calendar.getInstance();
		dateFormat.format(c);
		return c;
	}

	public List<Date> createCalendarDaysDescending(int amount) {
		Calendar date = Calendar.getInstance();
		List<Date> dates = new ArrayList<Date>();
		DateFormat formatter = new SimpleDateFormat("dd.MM.yy");
		for (int i = 0; i < amount + 1; i++) {
			dates.add(date.getTime());
			date.add(Calendar.DATE, -1);
			System.out.println(formatter.format(date.getTime()));
		}
		return dates;
	}

	public String convertDate(Calendar date) {
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
		return dateFormat.format(date);
	}

	private Comparator<Calendar> byDate = new Comparator<Calendar>() {
		@Override
		public int compare(Calendar d1, Calendar d2) {
			return d1.compareTo(d2);
		}
	};

}
