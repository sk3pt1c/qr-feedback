package de.nfomagic.qrfeedback.business.services;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;
import de.nfomagic.qrfeedback.business.objects.QrCode;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.AnswerRepository;
import de.nfomagic.qrfeedback.business.repositories.FeedbackCardRepository;
import de.nfomagic.qrfeedback.business.repositories.QrCodeRepository;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.feedbackCard.FeedbackCardGenerator;

@Stateless
public class FeedbackCardService {

	@Inject
	private Logger logger;

	@Inject
	FacesContext facesContext;

	@Inject
	private EntityManager em;

	@Inject
	private QuestionRepository questionRepository;

	@Inject
	FeedbackCardRepository feedbackCardRepository;

	@Inject
	private AnswerRepository answerRepositroy;

	@Inject
	QrCodeRepository qrCodeRepository;

	@Inject
	private FeedbackCardGenerator feedbackCardGenerator;

	// @PostConstruct
	// private void init() {
	// feedbackCardGeneratorNew = new FeedbackCardGeneratorNew();
	// }

	public void createFeedbackCards(Question question, int amount, boolean singleUsage) throws Exception {

		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
		String url = request.getRequestURL().toString();
		String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath()
				+ "/";
		String restPath = baseURL + "rest/codes/";

		// WORKAROUND FÜR: object references an unsaved transient instance -
		// save the transient instance before flushing
		question = questionRepository.find(question.getId());

		logger.info("Questiontitle is " + question.getQuestionText());

		List<FeedbackCard> feedbackCards = new ArrayList<FeedbackCard>();
		for (int i = 0; i < amount; i++) {
			FeedbackCard feedbackCard = new FeedbackCard(question);

			List<QrCode> qrCodes = new ArrayList<QrCode>();

			List<Answer> answers = answerRepositroy.findAllByQuestion(question);
			logger.info(answers.size() + " answers found for question with id " + question.getId());
			for (Answer answer : answers) {
				QrCode qrCode = new QrCode(answer, feedbackCard, restPath);
				qrCodes.add(qrCode);
				logger.info(
						"Added QR-Code with ID " + qrCode.getId() + " to FeedbackCard with ID " + feedbackCard.getId());
				logger.info("restPath is " + restPath);
			}

			feedbackCard.setQrCodes(qrCodes);
			feedbackCard.setSingleUsage(singleUsage);
			em.persist(feedbackCard);
			logger.info("Feedback-Card No. " + feedbackCard.getId() + " for Question No. " + question.getId()
					+ " created!");

			feedbackCards.add(feedbackCardRepository.findById(feedbackCard.getId()));

		}

		generatePdfsFromFeedbackCards(feedbackCards);

	}

	public void generatePdfsFromFeedbackCards(List<FeedbackCard> feedbackCards) throws Exception {
		feedbackCardGenerator.generateFeedbackCards(feedbackCards);
	}

	// @Deprecated
	// public void generatePdfFromFeedbackCard(FeedbackCard feedbackCard)
	// throws IOException, URISyntaxException, XDocReportException {
	// feedbackCardGenerator.generateCard(feedbackCard);
	// }

	public void mergeFeedbackCard(FeedbackCard feedbackCard) throws Exception {
		em.merge(feedbackCard);
		logger.info("Feedback-Card No. " + feedbackCard.getId() + " created!");
	}

	public void refreshFeedbackCard(FeedbackCard feedbackCard) {
		em.refresh(feedbackCard);
	}

	// TODO: Implementieren!
	public void deactivateQrCodesFromCard() {
		// List<QrCode> qrCodesFromCard = qrCode.getQrCard().getQrCodes();
		// List<QrCode> qrCodesFromCardForDeactivation = new
		// ArrayList<QrCode>();
		// for(QrCode qrCodeFromQrCard : qrCodesFromCard){
		// qrCodeFromQrCard.setActive(false);
		// qrCodesFromCardForDeactivation.add(qrCodeFromQrCard);
	}

}
