package de.nfomagic.qrfeedback.business.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;
import de.nfomagic.qrfeedback.business.objects.QrCode;
import de.nfomagic.qrfeedback.business.objects.Scan;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.AnswerRepository;
import de.nfomagic.qrfeedback.business.repositories.FeedbackCardRepository;
import de.nfomagic.qrfeedback.business.repositories.QrCodeRepository;
import de.nfomagic.qrfeedback.business.repositories.QuestionRepository;
import de.nfomagic.qrfeedback.business.repositories.ScanRepository;
import de.nfomagic.qrfeedback.exceptions.CardAlreadyScannedException;
import de.nfomagic.qrfeedback.exceptions.CodeAlreadyScannedException;

@Stateless
public class ScanService {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private QuestionRepository questionRepository;

	@Inject
	FeedbackCardService feedbackCardService;

	@Inject
	FeedbackCardRepository feedbackCardRepository;

	@Inject
	private AnswerRepository answerRepositroy;

	@Inject
	QrCodeRepository qrCodeRepository;

	@Inject
	AnswerService answerService;

	@Inject
	private ScanRepository scanRepository;

	private DateUtil dateUtil;

	private Scan scan;

	public ScanService() {

	}

	public Scan processScan(QrCode qrCode) throws CodeAlreadyScannedException, CardAlreadyScannedException {

		scan = new Scan();

		FeedbackCard feedbackCard = feedbackCardRepository.findById(qrCode.getFeedbackCard().getId());

		Question question = questionRepository.find(feedbackCard.getQuestion().getId());
		Answer answer = answerRepositroy.find(qrCode.getAnswer().getId());

		if (question.isMultipleChoice()) {
			if (qrCode.isActive()) {
				if (feedbackCard.isSingleUsage()) {
					qrCode.setActive(false);
				}
			} else {
				throw new CodeAlreadyScannedException();
			}
		} else {
			if (qrCode.isActive()) {

				// increment counter
				int counter = answer.getCounter();
				log.info("COUNTER BEFORE UPDATE: " + answer.getCounter());
				answer.setCounter(counter + 1);
				log.info("COUNTER AFTER UPDATE: " + answer.getCounter());

				// Persist incremented counter
				em.merge(answer);

				if (!feedbackCard.isSingleUsage()) {
					// Deactive QR-Codes from FeedbackCard
					List<QrCode> qrCodesFromCard = qrCodeRepository.findByFeedbackCard(feedbackCard);
					for (QrCode q : qrCodesFromCard) {
						q.setActive(false);
						em.merge(q);
					}
				}

				log.info("----QR-CODE WITH HASH " + qrCode.getHash() + " SCANNED (" + qrCode.getAnswer().getCounter()
						+ ")!----");

				scan.setAnswer(answer);

				log.info("Try to persist scan ...");
				em.persist(scan);
				log.info("Scan persisted with id " + scan.getId());

				return scan;

			} else {
				throw new CardAlreadyScannedException();
			}
		}
		return null;
	}

	public Map<Question, Map<String, Integer>> findScansForQuestionByDate() {

		Map<Question, Map<String, Integer>> questionMap = new HashMap<Question, Map<String, Integer>>();
		Map<String, Integer> dateMap;
		DateFormat formatter = new SimpleDateFormat("dd.MM.yy");

		dateUtil = new DateUtil();
		List<Date> dates = dateUtil.createCalendarDaysDescending(30);
		List<Question> questionsByUser = questionRepository.findAllActiveByMember();

		for (Question q : questionsByUser) {
			dateMap = new HashMap<String, Integer>();
			int numberOfScans;
			for (Date d : dates) {
				numberOfScans = 0;
				String dateString = formatter.format(d);
				List<Scan> scansByQuestion = findScansByQuestion(q);
				for (Scan s : scansByQuestion) {
					String scanDateString = formatter.format(s.getTimestamp());
					if (dateString.equals(scanDateString)) {
						numberOfScans++;
					}
				}
				log.info("About to put " + dateString + " and " + numberOfScans + " to dateMap!");
				dateMap.put(dateString, numberOfScans);
			}
			questionMap.put(q, dateMap);
		}
		return questionMap;
	}

	public List<Scan> findScansByQuestion(Question q) {
		List<Scan> scansByQuestion = scanRepository.findAllScansByQuestion(q);
		scansByQuestion.sort(byDate);
		return scansByQuestion;
	}

	private Comparator<Scan> byDate = new Comparator<Scan>() {
		@Override
		public int compare(Scan s1, Scan s2) {
			return s1.getTimestamp().compareTo(s2.getTimestamp());
		}
	};

}
