package de.nfomagic.qrfeedback.business.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import de.nfomagic.qrfeedback.business.objects.QrCode;
import de.nfomagic.qrfeedback.business.repositories.QrCodeRepository;
import de.nfomagic.qrfeedback.feedbackCard.qrcode.QrCodeImageGenerator;

@Stateless
@Named
public class QrCodeService {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	QrCodeRepository qrCodeRepository;

	@Inject
	QrCodeImageGenerator qrCodeGenerator;

	public void createQrCode(QrCode qrCode) throws Exception {
		em.persist(qrCode);
		log.info("QrCode with Hash " + qrCode.getHash() + " persisted!");
	}

	public void createQrCodes(List<QrCode> qrCodes) throws Exception {
		for (QrCode q : qrCodes) {
			em.persist(q);
			log.info("QrCode with ID " + q.getId() + " and Hash " + q.getHash()
					+ " persisted!");
		}
	}

}
