package de.nfomagic.qrfeedback.business.objects.surveyModule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotEmpty;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;
import de.nfomagic.qrfeedback.business.objects.idmModule.UserIdentity;

@Entity
public class Question implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String questionText;

	@NotNull
	private boolean multipleChoice;

	@NotNull
	private boolean active;

	// @OneToMany(targetEntity = Answer.class, mappedBy = "question", cascade =
	// CascadeType.ALL) <-- bidirectional OneToMany
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	// <-- unidirectional OneToMany (with @JoinColumn)
	@JoinColumn(name = "QUESTION_ID")
	private List<Answer> answers;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<FeedbackCard> feedbackCards;

	@ElementCollection
	@JoinTable(name = "ATTRIBUTE_VALUE_RANGE", joinColumns = @JoinColumn(name = "ID") )
	@MapKeyColumn(name = "RANGE_ID")
	@Column(name = "VALUE")
	private Map<String, String> params;

	@ManyToOne()
	@JoinColumn(name = "USER_ID")
	private UserIdentity user;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	@Transient
	private int totalAnswerCount = 0;

	public Question() {
		super();
		answers = new ArrayList<Answer>();
	}

	public Question(UserIdentity actualUser) {
		super();
		this.user = actualUser;
	}

	public Question(String questionText, boolean multipleChoice, UserIdentity actualUser) {
		super();
		this.questionText = questionText;
		this.active = true;
		this.multipleChoice = multipleChoice;
		this.user = actualUser;
	}

	public Long getId() {
		return id;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public boolean isMultipleChoice() {
		return multipleChoice;
	}

	public void setMultipleChoice(boolean multipleChoice) {
		this.multipleChoice = multipleChoice;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<FeedbackCard> getFeedbackCards() {
		return feedbackCards;
	}

	public void setFeedbackCards(List<FeedbackCard> feedbackCards) {
		this.feedbackCards = feedbackCards;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public Integer getTotalAnswerCount() {
		for (Answer a : answers) {
			totalAnswerCount += a.getCounter();
			System.out.println("Added " + a.getCounter());
		}
		System.out.println("Total Answer count is " + totalAnswerCount);
		return totalAnswerCount;
	}

	public void setTotalAnswerCount(Integer totalAnswerCount) {
		this.totalAnswerCount = totalAnswerCount;
	}

	public UserIdentity getUser() {
		return user;
	}

	public void setUser(UserIdentity user) {
		this.user = user;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

}
