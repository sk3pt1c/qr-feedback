package de.nfomagic.qrfeedback.business.objects.deviceCheckModule;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Device implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String deviceName;

	private String deviceVendor;

	private String lastKnownIpAdress;

	@OneToOne
	private OperatingSystem operatingSystem;

	@OneToMany
	private List<Browser> browsers;

	public Device() {
	}

	public Device(String deviceName, String deviceVendor, String lastKnownIpAdress, OperatingSystem operatingSystem,
			List<Browser> browsers) {
		this.deviceName = deviceName;
		this.deviceVendor = deviceVendor;
		this.lastKnownIpAdress = lastKnownIpAdress;
		this.operatingSystem = operatingSystem;
		this.browsers = browsers;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceVendor() {
		return deviceVendor;
	}

	public void setDeviceVendor(String deviceVendor) {
		this.deviceVendor = deviceVendor;
	}

	public String getLastKnownIpAdress() {
		return lastKnownIpAdress;
	}

	public void setLastKnownIpAdress(String lastKnownIpAdress) {
		this.lastKnownIpAdress = lastKnownIpAdress;
	}

	public OperatingSystem getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(OperatingSystem operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public List<Browser> getBrowsers() {
		return browsers;
	}

	public void setBrowsers(List<Browser> browsers) {
		this.browsers = browsers;
	}

	public Long getId() {
		return id;
	}

}
