package de.nfomagic.qrfeedback.business.objects.idmModule;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Person implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String name;
	private int taxClass;
	private Date birthDate;
	private Set<String> languageSkills = new HashSet<String>();

	public Person(String name, int taxClass, Date birthDate) {
		this.name = name;
		this.taxClass = taxClass;
		this.birthDate = birthDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLanguageSkills(Set<String> languageSkills) {
		this.languageSkills = languageSkills;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTaxClass() {
		return taxClass;
	}

	public void setTaxClass(int taxClass) {
		this.taxClass = taxClass;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String[] getLanguageSkills() {
		return languageSkills.toArray(new String[languageSkills.size()]);
	}

	public void addLanguageSkill(String languageSkill) {
		languageSkills.add(languageSkill);
	}
}