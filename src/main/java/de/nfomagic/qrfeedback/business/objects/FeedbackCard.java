package de.nfomagic.qrfeedback.business.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;

@Entity
public class FeedbackCard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private boolean singleUsage;

	private String tags;
	private String author;

	// TODO nullable = true pruefen
	@ManyToOne()
	private Question question;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "FEEDBACKCARD_ID")
	private List<QrCode> qrCodes;

	public FeedbackCard() {
		super();
		qrCodes = new ArrayList<QrCode>();
	}

	public FeedbackCard(Question question) {
		super();
		this.question = question;
		this.qrCodes = new ArrayList<QrCode>();
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public boolean isSingleUsage() {
		return singleUsage;
	}

	public void setSingleUsage(boolean singleUsage) {
		this.singleUsage = singleUsage;
	}

	public List<QrCode> getQrCodes() {
		return qrCodes;
	}

	public void setQrCodes(List<QrCode> qrCodes) {
		this.qrCodes = qrCodes;
	}

	public long getId() {
		return id;
	}

}
