package de.nfomagic.qrfeedback.business.objects.idmModule;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.picketlink.idm.jpa.annotations.AttributeValue;
import org.picketlink.idm.jpa.annotations.Identifier;
import org.picketlink.idm.jpa.annotations.OwnerReference;
import org.picketlink.idm.jpa.annotations.entity.IdentityManaged;
import org.picketlink.idm.jpa.model.sample.simple.PartitionTypeEntity;
import org.picketlink.idm.model.annotation.AttributeProperty;
import org.picketlink.idm.model.basic.User;

@IdentityManaged(User.class)
@Entity
public class UserIdentity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6426439892842298661L;

	@Id
	@Identifier
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@AttributeValue
	private String loginName;

	@AttributeValue
	private String firstName;

	@AttributeValue
	private String lastName;

	@AttributeValue
	private String email;

	@AttributeProperty
	private String company;

	@AttributeValue
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@AttributeValue
	private boolean enabled;

	@OwnerReference
	@ManyToOne
	private PartitionTypeEntity partition;

	public UserIdentity() {
		this.enabled = true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public PartitionTypeEntity getPartition() {
		return partition;
	}

	public void setPartition(PartitionTypeEntity partition) {
		this.partition = partition;
	}

}