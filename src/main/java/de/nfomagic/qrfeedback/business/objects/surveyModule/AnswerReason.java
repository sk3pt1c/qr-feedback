package de.nfomagic.qrfeedback.business.objects.surveyModule;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import de.nfomagic.qrfeedback.business.objects.Scan;

/**
 * Entity implementation class for Entity: AnswerReason
 *
 */
@Entity
public class AnswerReason implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String answerReason;

	@OneToOne
	private Scan scan;

	public AnswerReason() {
	}

	public AnswerReason(String answerReason, Scan scan) {
		this.answerReason = answerReason;
		this.scan = scan;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAnswerReason() {
		return answerReason;
	}

	public void setAnswerReason(String answerReason) {
		this.answerReason = answerReason;
	}

	public Scan getScan() {
		return scan;
	}

	public void setScan(Scan scan) {
		this.scan = scan;
	}

}
