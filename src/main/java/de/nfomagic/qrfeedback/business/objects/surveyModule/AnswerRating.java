package de.nfomagic.qrfeedback.business.objects.surveyModule;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import de.nfomagic.qrfeedback.business.objects.Scan;

/**
 * Entity implementation class for Entity: AnswerReason
 *
 */
@Entity
public class AnswerRating implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer rating;

	@OneToOne
	private Scan scan;

	public AnswerRating() {
	}

	public AnswerRating(Integer rating, Scan scan) {
		this.rating = rating;
		this.scan = scan;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Scan getScan() {
		return scan;
	}

	public void setScan(Scan scan) {
		this.scan = scan;
	}

}
