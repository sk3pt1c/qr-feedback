package de.nfomagic.qrfeedback.business.objects;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.feedbackCard.qrcode.QrCodeHashGenerator;
import de.nfomagic.qrfeedback.feedbackCard.qrcode.QrCodeImageGenerator;
import org.hibernate.validator.constraints.Length;

@Entity
public class QrCode implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    // Alphanumeric id e.g. Fu4ih3d0FbN5lD3j
    private String hash;

    // Nur bei Multiple-Choice benötigt TODO: Vererbung realisieren!
    private boolean active;

    @Lob
    //	@Type(type = "org.hibernate.type.BinaryType") // TODO: deactivate Lob when activating @Type
    @Column(nullable = false)
    private byte[] qrImage;

    @Transient
    private QrCodeImageGenerator qrCodeImageGenerator;

    @Transient
    private QrCodeHashGenerator qrCodeHashGenerator;

    @ManyToOne()
    private Answer answer;

    @ManyToOne()
    private FeedbackCard feedbackCard;

    public QrCode() {
    }

    public QrCode(Answer answer, FeedbackCard feedbackCard, String restPath) {
        // this(); --> Calling default constructor --> avoiding code redundancy
        qrCodeImageGenerator = new QrCodeImageGenerator();
        qrCodeHashGenerator = new QrCodeHashGenerator();
        this.hash = qrCodeHashGenerator.generateUniqueHashValue();
        this.qrImage = qrCodeImageGenerator.generateCode(restPath + hash);
        this.active = true;
        this.answer = answer;
    }

    public long getId() {
        return id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public byte[] getQrImage() {
        return qrImage;
    }

    public void setQrImage(byte[] qrImage) {
        this.qrImage = qrImage;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public FeedbackCard getFeedbackCard() {
        return feedbackCard;
    }

    public void setFeedbackCard(FeedbackCard feedbackCard) {
        this.feedbackCard = feedbackCard;
    }

}
