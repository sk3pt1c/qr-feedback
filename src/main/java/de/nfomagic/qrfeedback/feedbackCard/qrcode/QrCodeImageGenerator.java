package de.nfomagic.qrfeedback.feedbackCard.qrcode;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QrCodeImageGenerator implements QrCodeGenerator {

	@Inject
	Logger log;

	@Inject
	FacesContext facesContext;

	public byte[] generateCode(String restURI) {

		QRCodeWriter writer = new QRCodeWriter();

		Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

		// Create a qr code with the url as content and a size of 500x500 px
		BitMatrix bitMatrix = null;
		try {
			bitMatrix = writer.encode(restURI, BarcodeFormat.QR_CODE, 500, 500, hints);
		} catch (WriterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MatrixToImageConfig config = new MatrixToImageConfig(MatrixToImageConfig.BLACK, MatrixToImageConfig.WHITE);

		BufferedImage qrCodeImage = MatrixToImageWriter.toBufferedImage(bitMatrix, config);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] imageInByte = {};
		try {
			ImageIO.write(qrCodeImage, "png", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Return QR image
		return imageInByte;
	}

	public BufferedImage addLogo(BufferedImage qrImage, BufferedImage logoImage) {

		// Resize image to 25% of the QR-Image
		logoImage = resizeImage(logoImage, qrImage.getWidth() / 4, qrImage.getHeight() / 4);

		// Calculate the delta height and width between QR code and logo
		int deltaHeight = qrImage.getHeight() - logoImage.getHeight();
		int deltaWidth = qrImage.getWidth() - logoImage.getWidth();

		// Initialize combined image
		BufferedImage qrImageWithCode = new BufferedImage(qrImage.getHeight(), qrImage.getWidth(),
				BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = (Graphics2D) qrImageWithCode.getGraphics();

		// Write QR code to new image at position 0/0
		g.drawImage(qrImage, 0, 0, null);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

		// Write logo into combine image at position (deltaWidth / 2) and
		// (deltaHeight / 2). Background: Left/Right and Top/Bottom must be
		// the same space for the logo to be centered
		g.drawImage(logoImage, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2), null);

		return qrImageWithCode;
	}

	private BufferedImage resizeImage(BufferedImage image, int width, int height) {

		int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image.getType();

		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(image, 0, 0, width, height, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		return resizedImage;
	}

}
