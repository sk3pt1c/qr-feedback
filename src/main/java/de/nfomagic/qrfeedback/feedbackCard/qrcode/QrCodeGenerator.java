package de.nfomagic.qrfeedback.feedbackCard.qrcode;

public interface QrCodeGenerator {

	public byte[] generateCode(String url);
	
}
