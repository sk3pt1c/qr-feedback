package de.nfomagic.qrfeedback.feedbackCard;

import java.util.List;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;

public interface FeedbackCardGenerator {

	public void generateFeedbackCards(List<FeedbackCard> feedbackCards)
			throws Exception;

}
