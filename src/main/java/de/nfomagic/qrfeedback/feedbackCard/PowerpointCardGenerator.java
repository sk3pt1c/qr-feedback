package de.nfomagic.qrfeedback.feedbackCard;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFPictureShape;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

import de.nfomagic.qrfeedback.business.objects.FeedbackCard;
import de.nfomagic.qrfeedback.business.objects.QrCode;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Answer;
import de.nfomagic.qrfeedback.business.objects.surveyModule.Question;
import de.nfomagic.qrfeedback.business.repositories.AnswerRepository;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

@Stateless
public class PowerpointCardGenerator implements FeedbackCardGenerator {

	@Inject
	Logger log;

	private static final String DESTINATION_PATH = System.getProperty("jboss.server.data.dir", "uploads") + "\\";

	// TODO: Hackbarkeit prüfen --> Ist es klug die fertige PPTX-Datei hier zu
	// speichern???
	private static final String FEEDBACKCARD_OUTPUT_PATH = DESTINATION_PATH + "FeedbackCards.pptx";

	private static final String TMP_IMAGE_PATH = DESTINATION_PATH + "tmp/";

	private static final String TEMPLATE_PATH = "templates/MasterLayout.pptx";

	XMLSlideShow powerpoint = null;

	@Inject
	private AnswerRepository answerRepository;

	public PowerpointCardGenerator() {
		try {
			powerpoint = new XMLSlideShow(this.getClass().getClassLoader().getResourceAsStream(TEMPLATE_PATH));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateFeedbackCards(List<FeedbackCard> feedbackCards) throws Exception {

		log.info("About to generate " + feedbackCards.size() + " FeedbackCards");

		XSLFSlideMaster[] masterLayout = powerpoint.getSlideMasters();

		// Hier können Designs ausgewählt werden
		XSLFSlideLayout[] customLayouts = masterLayout[0].getSlideLayouts();

		XSLFSlideLayout fourCodeLayout = customLayouts[0];
		XSLFSlideLayout threeCodeLayout = customLayouts[2];
		XSLFSlideLayout twoCodeLayout = customLayouts[1];

		int i = 0;

		// Folien erstellen
		// TODO: Auswirkungen auf Performance überprüfen -->
		// f.getQuestion.getAnswers wird für jede FBCARD ausgeführt...
		for (FeedbackCard f : feedbackCards) {

			List<File> images = convertQrCodesToFiles(f.getQrCodes());
			List<Answer> answers = answerRepository.findAllByQuestion(f.getQuestion());
			int answerCount = answers.size();

			log.info("AnswerCount is " + answerCount);

			if (answerCount == 2) {
				// slides.add(powerpoint.createSlide(twoCodeLayout));
				XSLFSlide twoCodeSlide = powerpoint.createSlide(twoCodeLayout);
				log.info("Generated slide with twoCodeLayout");
				log.info(getImagePlaceholderCount(twoCodeSlide) + " image placeholders");
				replaceImages(twoCodeSlide, images, i);
			} else if (answerCount == 3) {
				// slides.add(powerpoint.createSlide(threeCodeLayout));
				XSLFSlide threeCodeSlide = powerpoint.createSlide(threeCodeLayout);
				log.info("Generated slide with threeCodeLayout");
				log.info(getImagePlaceholderCount(threeCodeSlide) + " image placeholders");
				replaceImages(threeCodeSlide, images, i);
			} else if (answerCount == 4) {
				// slides.add(powerpoint.createSlide(fourCodeLayout));
				XSLFSlide fourCodeSlide = powerpoint.createSlide(fourCodeLayout);
				log.info("Generated slide with fourCodeLayout");
				log.info(getImagePlaceholderCount(fourCodeSlide) + " image placeholders");
				replaceImages(fourCodeSlide, images, i);
			}

			i = i + answerCount;

		}

		saveFile(powerpoint);

		// TODO: Dynamisch machen! --> Mit feedbackCards.get(0).getQuestion
		// gehören alle in der PPTX vorkommenden Folien zu einer Frage -->
		// Mehrere Fragen sollen zukünftig erlaubt sein!
		replaceTextPlaceholders(new FileInputStream(FEEDBACKCARD_OUTPUT_PATH), feedbackCards.get(0).getQuestion());
	}

	private int getImagePlaceholderCount(XSLFSlide slide) {

		int imagePlaceholderCount = 0;

		Iterator<XSLFShape> shapes_it = new ArrayList<XSLFShape>(Arrays.asList(slide.getShapes())).iterator();

		while (shapes_it.hasNext()) {
			XSLFShape shape = shapes_it.next();
			if (shape instanceof XSLFTextShape && shape.getShapeName().contains("Bildplatzhalter")) {
				imagePlaceholderCount++;
			}
		}

		return imagePlaceholderCount;
	}

	private void replaceImages(XSLFSlide slide, List<File> images, int index) throws Exception {

		Iterator<XSLFShape> shapes_it = new ArrayList<XSLFShape>(Arrays.asList(slide.getShapes())).iterator();

		int i = 0;
		while (shapes_it.hasNext()) {
			XSLFShape shape = shapes_it.next();
			if (shape instanceof XSLFTextShape && shape.getShapeName().contains("Bildplatzhalter")) {

				// picture placeholder
				Rectangle2D anchor = shape.getAnchor();

				// remove the placehoder shape
				slide.removeShape(shape);

				// add something new
				// TODO: Index dynamisch ermitteln! --> pptx mit hintergrundbild
				// hat bereits eine datei
				PackagePartName partName = PackagingURIHelper.createPartName("/ppt/media/image" + (index + 5) + ".png");

				PackagePart part = powerpoint.getPackage().createPart(partName, "image/png");

				OutputStream partOs = part.getOutputStream();

				log.info("Trying to add image from " + images.get(i).getAbsolutePath());

				// write image to .pptx
				FileInputStream fis2 = new FileInputStream(images.get(i));

				byte buf[] = new byte[1024];
				for (int readBytes; (readBytes = fis2.read(buf)) != -1; partOs.write(buf, 0, readBytes))
					;
				fis2.close();
				partOs.close();

				// insert a picture indead
				XSLFPictureShape pic = slide.createPicture(index+4);
				pic.setAnchor(anchor);
				index++;
				i++;
			}

		}

	}

	// TODO: Prüfen ob InMemory-Lösung mit OutputStream-->Inputstream besser ist
	private void saveFile(XMLSlideShow ppt) {
		// saving the changes
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(new File(FEEDBACKCARD_OUTPUT_PATH));
			ppt.write(out);
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void replaceTextPlaceholders(FileInputStream feedbackCardTemplateStream, Question question)
			throws IOException, XDocReportException {

		// 1) Load PPTX file by filling Velocity template engine and cache
		// it to the registry
		IXDocReport report = XDocReportRegistry.getRegistry().loadReport(feedbackCardTemplateStream,
				TemplateEngineKind.Velocity);

		// 2) Create context Java model
		IContext context = report.createContext();
		context.put("questionText", question.getQuestionText());

		// log.info("questionRepo is " + questionRepository);
		// refresh question entity TODO: ohne wär besser!

		// List<Answer> answers = answerRepository.findAllByQuestion(question);
		List<Answer> answers = answerRepository.findAllByQuestion(question);

		int answerNo = 1;
		for (Answer a : answers) {
			log.info("About to add answer " + a.getAnswerText() + "to context as answerText" + answerNo);
			context.put("answerText" + answerNo, a.getAnswerText());
			answerNo++;
		}

		// 3) Generate report by merging Java model with the PPTX
		// TODO: NEBENLÄUFIGKEIT GARANETIEREN --> GLEICHZEITIGE ERSTELLUNG VON
		// FBCARDS
		OutputStream out = new FileOutputStream(new File(FEEDBACKCARD_OUTPUT_PATH));
		report.process(context, out);

		out.close();
		feedbackCardTemplateStream.close();
	}

	// Used to convert qrCodeImage to .png file
	private File convertByteArrayToFile(byte[] byteArray) throws IOException {
		File pngImage = new File(TMP_IMAGE_PATH + RandomUtils.nextInt(new Random()) + ".png");
		FileUtils.writeByteArrayToFile(pngImage, byteArray);
		return pngImage;
	}

	// Used to convert qrCodeImages to .png files
	private List<File> convertQrCodesToFiles(List<QrCode> qrCodes) throws IOException {

		List<File> pngImages = new ArrayList<File>();

		for (QrCode q : qrCodes) {
			File pngImage = new File(TMP_IMAGE_PATH + RandomUtils.nextInt(new Random()) + ".png");
			FileUtils.writeByteArrayToFile(pngImage, q.getQrImage());
			pngImages.add(pngImage);
		}
		return pngImages;
	}

}
