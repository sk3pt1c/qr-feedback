/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.nfomagic.qrfeedback.authorization;

import java.util.logging.Logger;

import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.picketlink.Identity;
import org.picketlink.Identity.AuthenticationResult;
import org.picketlink.credential.DefaultLoginCredentials;

import de.nfomagic.qrfeedback.business.repositories.UserIdentityRepository;
import de.nfomagic.qrfeedback.producer.ActualUserProducer;

@Model
public class LoginController {

	@Inject
	private Identity identity;

	@Inject
	DefaultLoginCredentials credentials;

	@Inject
	private FacesContext facesContext;

	@Inject
	ActualUserProducer actualUserBean;

	@Inject
	private UserIdentityRepository userIdentityRepository;

	@Inject
	private Logger log;

	public String login() {

		if (identity.isLoggedIn()) {
			return "/protected/dashboard.jsf?faces-redirect=true";
		}

		AuthenticationResult result = identity.login();
		if (AuthenticationResult.SUCCESS.equals(result)) {
			log.info("User " + credentials.getUserId() + " logged in");

			// SET ACTUAL USER FOR SESSION
			actualUserBean.setActualUser(userIdentityRepository.findByUsername(credentials.getUserId()));

			return "protected/dashboard";
		} else {
			facesContext.addMessage(null,
					new FacesMessage("Authentication was unsuccessful.  Please check your username and password "
							+ "before trying again."));
			log.info("Login for User failed!");
			return null;
		}
	}
}
