package de.nfomagic.qrfeedback.authorization;

import javax.enterprise.event.Observes;

import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.SecurityConfigurationEvent;

public class HttpSecurityConfiguration {

	/**
	 * Configures Http Security.
	 * 
	 * @param event
	 *            the SecurityConfigurationEvent.
	 */
	public void configureHttpSecurity(@Observes SecurityConfigurationEvent event) {
		SecurityConfigurationBuilder builder = event.getBuilder();
		builder.http()
			.forPath("/protected/*").authenticateWith().form().loginPage("/login.xhtml").authenticationUri("/login.xhtml").errorPage("/loginFailed.html")
			.forPath("/logout").logout().redirectTo("/login.xhtml")
			.forPath("/admin/*").authorizeWith().role("Administrator").redirectTo("/accessDenied.xhtml").whenForbidden();
	}

	// public void configureHttpSecurity(@Observes SecurityConfigurationEvent
	// event) {
	// SecurityConfigurationBuilder builder = event.getBuilder();
	//
	// builder.http().forPath("/*.jsf").authenticateWith().form().loginPage("/home.html").errorPage("/loginFailed.html");
	// builder.http().forPath("/logout").logout().redirectTo("index.html");
	//
	// }

}