package de.nfomagic.qrfeedback.exceptions;

public class NoSurveySelectedException extends Exception {

	private static final long serialVersionUID = 4664456874499611218L;

	private static final String errorCode = "NO_SURVEY_SELECTED";

	public NoSurveySelectedException() {
		super("Der Frage wurden keine Antworten zugeordnet!");
	}

	public String getErrorCode() {
		return errorCode;
	}

}
