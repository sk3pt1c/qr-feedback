package de.nfomagic.qrfeedback.exceptions;

public class WrongAnswerCountException extends Exception {

	private static final long serialVersionUID = 4664456874499611218L;

	private static final String errorCode = "WRONG_ANSWER_COUNT";

	public WrongAnswerCountException() {
		super("Fragen dürfen zwischen 2 und 4 Antworten enthalten!");
	}

	public String getErrorCode() {
		return errorCode;
	}

}
