package de.nfomagic.qrfeedback.exceptions;

public class CodeAlreadyScannedException extends Exception {

	private static final long serialVersionUID = 4664456874499611218L;

	private static final String errorCode = "CODE_ALREADY_SCANNED";

	public CodeAlreadyScannedException() {
		super("Dieser Code wurde bereits gescannt!");
	}

	public String getErrorCode() {
		return errorCode;
	}

}
