package de.nfomagic.qrfeedback.exceptions;

public class CardAlreadyScannedException extends Exception {

	private static final long serialVersionUID = 4664456874499611218L;

	private static final String errorCode = "CARD_ALREADY_SCANNED";

	public CardAlreadyScannedException() {
		super("Ein Code von dieser Karte wurde bereits gescannt!");
	}

	public String getErrorCode() {
		return errorCode;
	}

}
