package de.nfomagic.qrfeedback.exceptions;

public class NoAnswersForQuestionException extends Exception {

	private static final long serialVersionUID = 4664456874499611218L;

	private static final String errorCode = "NO_ANSWERS_FOR_QUESTION";

	public NoAnswersForQuestionException() {
		super("Der Frage wurden keine Antworten zugeordnet!");
	}

	public String getErrorCode() {
		return errorCode;
	}

}
